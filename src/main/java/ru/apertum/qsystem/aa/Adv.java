/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.aa;

import java.util.Date;


/**
 * @author Evgeniy Egorov
 */
public class Adv {

    public final long id;
    public final long num;
    public final Date date;

    public Adv(long id, long num, Date date) {
        this.id = id;
        this.num = num;
        this.date = date;
    }

}
