/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.aa;


import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.lookup.MainMapLookup;

import java.nio.charset.Charset;

/**
 * @author Evgeniy Egorov
 */
public class Log {

    public static final String KEY_IDE = "ide";
    public final Logger logger;
    public static boolean isIDE = false;

    private Log() {
        Charset charset = Charset.forName("UTF-8");
        if (!isIDE && SystemUtils.IS_OS_WINDOWS) { // Операционка и бинс
            charset = Charset.forName("cp866");
        }
        final String[] args = {"aa.file.info.trace", charset.name()};
        MainMapLookup.setMainArguments(args);
        logger = LogManager.getLogger("aa.file.info.trace");
        logger.info("СТАРТ ЛОГИРОВАНИЯ. Логгер: " + logger.getName());
    }

    public static Log getInstance() {
        return LogHolder.INSTANCE;
    }

    private static class LogHolder {

        private static final Log INSTANCE = new Log();
    }
}
