/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.aa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import ru.apertum.qsystem.aa.exeptions.ServerException;

/**
 * @author Evgeniy Egorov
 */
public class Run {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        for (String string : args) {
            if (Log.KEY_IDE.equalsIgnoreCase(string)) {
                Log.isIDE = true;
            }
        }
        Log.getInstance().logger.info("Start process..");

        final DateFormat format_date = new SimpleDateFormat(Props.getInstance().getDateFormat());
        final LinkedList<Adv> rec = new LinkedList<>();

        final Configurations configs = new Configurations();
        final FileBasedConfigurationBuilder<PropertiesConfiguration> builder = configs.propertiesBuilder(FAdvAdapter.IDS_FILE);
        final PropertiesConfiguration ids;
        try {
            ids = builder.getConfiguration();
        } catch (ConfigurationException ex) {
            throw new RuntimeException(ex);
        }
        // ролики в папке
        final String[] videoFiles = new File(Props.getInstance().getDataFolder()).list();
        int i = 0;
        // Бежим по всем файлам в папке
        for (String string : videoFiles) {
            videoFiles[i++] = Props.getInstance().getDataFolder() + (Props.getInstance().getDataFolder().substring(Props.getInstance().getDataFolder().length() - 1).equals(File.separator) ? "" : File.separator) + string;
            Log.getInstance().logger.info("   **********************************************************************");
            Log.getInstance().logger.info(videoFiles[i - 1]);

            // Вот файл. Прочитаем его.
            String data = "";
            final BufferedReader reader;
            try {
                reader = new BufferedReader(new FileReader(videoFiles[i - 1]));
                String line;
                while ((line = reader.readLine()) != null) {
                    data = data + "\n" + line;
                }
                //System.out.println(data);
            } catch (FileNotFoundException ex) {
            } catch (IOException ex) {
            }

            // Из прочтенного файла выберем нужные данные регулярками.
            final Pattern pattern = Pattern.compile(Props.getInstance().getListRegexp());
            final Matcher matcher = pattern.matcher(data);
            while (matcher.find()) {
                Log.getInstance().logger.trace(matcher.group());

                List<String> ss = Props.getInstance().getTimeRegexp();
                String base = matcher.group();
                for (String s : ss) {
                    final Pattern pT = Pattern.compile(s.trim());
                    final Matcher mT = pT.matcher(base);
                    final boolean f = mT.find();
                    if (!f) {
                        throw new ServerException("Регулярка \"" + s.trim() + "\" поиска времени должна находить хоть что-то. Ошибка регулярки");
                    }
                    base = mT.group();
                }
                Log.getInstance().logger.trace(" Time=" + base);
                final Date date;
                try {
                    date = format_date.parse(base);
                } catch (ParseException ex) {
                    throw new ServerException("Дата \"" + base + "\" каакя-то не датая. Ошибка преобразования данных");
                }

                ss = Props.getInstance().getServiceIdRegexp();
                base = matcher.group();
                for (String s : ss) {
                    final Pattern pT = Pattern.compile(s.trim());
                    final Matcher mT = pT.matcher(base);
                    final boolean f = mT.find();
                    if (!f) {
                        throw new ServerException("Регулярка \"" + s.trim() + "\" поиска ID услуги должна находить хоть что-то. Ошибка регулярки");
                    }
                    base = mT.group();
                }
                Log.getInstance().logger.trace(" ServiceID=" + base);
                final String qsysID = ids.getString(base, "");
                final String outID = base;

                ss = Props.getInstance().getNumberRegexp();
                base = matcher.group();
                for (String s : ss) {
                    final Pattern pT = Pattern.compile(s.trim());
                    final Matcher mT = pT.matcher(base);
                    final boolean f = mT.find();
                    if (!f) {
                        throw new ServerException("Регулярка \"" + s.trim() + "\" поиска номера должна находить хоть что-то. Ошибка регулярки");
                    }
                    base = mT.group();
                }
                Log.getInstance().logger.trace(" Number=" + base);
                final long num = Long.parseLong(base);
                if ("".equals(qsysID)) {
                    Log.getInstance().logger.warn("Приехавшая услуга id=" + outID + " не привязана! Ошибка преобразования данных");
                } else {
                    Log.getInstance().logger.trace("QService.id=" + qsysID);

                    final long id = Long.parseLong(qsysID);

                    if (date.after(new Date())) {
                        rec.add(new Adv(id, num, date));
                    } else {
                        Log.getInstance().logger.warn("Устаревшая дата " + date + " предварительной записи " + num);
                    }
                }
            }
        }

        // Название драйвера
        Log.getInstance().logger.info("Загрузка данных по предварительно записанным в БД QSystem");
        final String driverName = "com.mysql.jdbc.Driver";
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException ex) {
            throw new ServerException("Не найдена библиотека. Инициализация", ex);
        }
        Connection con = null;

        final String SQL_INSERT = "INSERT INTO advance (id, service_id, advance_time, input_data) VALUES (?,?,?,'');";
        final String SQL_DEL = "DELETE FROM advance WHERE id=?;";
        final String SQL_DEL2 = "DELETE FROM advance WHERE advance_time<?;";
        try {
            //con = cpds.getConnection();
            con = DriverManager.getConnection(Props.getInstance().getURL(), Props.getInstance().getUser(), Props.getInstance().getPassword());
            final PreparedStatement statement = con.prepareStatement(SQL_INSERT);
            final PreparedStatement stD = con.prepareStatement(SQL_DEL);
            final PreparedStatement stD2 = con.prepareStatement(SQL_DEL2);
            stD2.setTimestamp(1, new java.sql.Timestamp(new Date().getTime()));
            Log.getInstance().logger.trace("DEL old records");
            stD2.executeUpdate();

            for (Adv adv : rec) {
                stD.setLong(1, adv.num);
                Log.getInstance().logger.trace("DEL advance.id=" + adv.num);
                stD.executeUpdate();

                statement.setLong(1, adv.num);
                statement.setLong(2, adv.id);
                statement.setTimestamp(3, new java.sql.Timestamp(adv.date.getTime()));
                Log.getInstance().logger.trace("INSERT advance.id=" + adv.num + " serv.id=" + adv.id + " date=" + adv.date);
                statement.executeUpdate();
            }

        } catch (SQLException ex) {
            throw new ServerException("Инициализация неудачна. Инициализация", ex);
        } catch (Exception ex) {
            throw new ServerException("Неопознанная ошибка в дебрях создания соенинения. Инициализация", ex);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                throw new ServerException("Закрытие соединеня неудачно. Инициализация", ex);
            }
        }

        Log.getInstance().logger.info("Загрузка списка предварительно записанных и отправка в БД QSystem завершена. Перекачка");

    }
}
