/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.aa;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;
import java.util.List;


/**
 * @author Evgeniy Egorov
 */
public class Props {

    private final static String CONFIG_FILE = "config/config.properties";
    final Parameters params = new Parameters();
    final FileBasedConfigurationBuilder<PropertiesConfiguration> builder = new FileBasedConfigurationBuilder<PropertiesConfiguration>(
            PropertiesConfiguration.class).configure(params.fileBased().setListDelimiterHandler(new DefaultListDelimiterHandler('@'))
            .setFile(new File(CONFIG_FILE)));
    final PropertiesConfiguration config;

    private Props() {
        Log.getInstance().logger.info("Загрузка настроек " + CONFIG_FILE);
        try {
            builder.setAutoSave(true);
            config = builder.getConfiguration();
        } catch (ConfigurationException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static Props getInstance() {
        return PropsHolder.INSTANCE;
    }

    private static class PropsHolder {

        private static final Props INSTANCE = new Props();
    }

    public void save() {
        try {
            builder.save();
        } catch (ConfigurationException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String getURL() {
        return config.getString("db_url");
    }

    public String getUser() {
        return config.getString("db_user");
    }

    public String getPassword() {
        return config.getString("db_password");
    }

    public String getDataFolder() {
        return config.getString("data_folder");
    }

    public String getDateFormat() {
        return config.getString("re_time_format", "dd.MM.yyyy HH:mm:ss");
    }

    public void setURL(String url) {
        config.clearProperty("db_url");
        config.addProperty("db_url", url);
    }

    public void setUser(String user) {
        config.clearProperty("db_user");
        config.addProperty("db_user", user);
    }

    public void setPassword(String password) {
        config.clearProperty("db_password");
        config.addProperty("db_password", password);
    }

    public void setDataFolder(String dataFolder) {
        config.clearProperty("data_folder");
        config.addProperty("data_folder", dataFolder);
    }

    public void setDateFormat(String dateFormat) {
        config.clearProperty("re_time_format");
        config.addProperty("re_time_format", dateFormat);
    }

    public List<String> getServiceIdRegexp() {
        return config.getList(String.class, "re_serviceid");
    }

    public List<String> getNumberRegexp() {
        return config.getList(String.class, "re_number");
    }

    public List<String> getTimeRegexp() {
        return config.getList(String.class, "re_time");
    }

    public String getListRegexp() {
        return config.getString("re_list");
    }

    public void setListRegexp(String dataFolder) {
        config.clearProperty("re_list");
        config.addProperty("re_list", dataFolder);
    }

    public void setServiceIdRegexp(String[] list) {
        config.clearProperty("re_serviceid");
        config.addProperty("re_serviceid", list);
    }

    public void setNumberRegexp(String[] list) {
        config.clearProperty("re_number");
        config.addProperty("re_number", list);
    }

    public void setTimeRegexp(String[] list) {
        config.clearProperty("re_time");
        config.addProperty("re_time", list);
    }
}
